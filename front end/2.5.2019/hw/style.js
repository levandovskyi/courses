let nav = document.getElementById('nav');
let links = nav.getElementsByClassName('link');

let pages = document.getElementById('pages');
let btns = pages.getElementsByTagName('button');

let icons = document.getElementById('icons');
let linksFooter = icons.getElementsByTagName('a');

function removeActive(a,b) {
  for (let i = 0; i < a.length; i++) {
    a[i].className = b;
  }
}

function toggleActive(a, b, c) {
  for (let i = 0; i < a.length; i++) {
    a[i].addEventListener('click', (e) => {
      removeActive(a, b);
      e.target.classList.toggle(c);
    })
  }
}


toggleActive(links, 'link', 'active');
toggleActive(btns, 'btn', 'activeBtn');
toggleActive(linksFooter, 'linkF', 'activeF');

