//list of variables in the project
let addCar = document.getElementById('add');
let raceBtn = document.getElementById('race');
let time = document.getElementById('time');
let results = document.getElementById('results');
let resultParagraph = document.getElementById('resP');
let finish = document.getElementById('finish');
let random = document.getElementById('random');
let speed = Number(document.getElementById('speed'));
let resetDiv = document.getElementById('resetDiv');
let reset = document.getElementById('reset');
const carNames = ['BMW', 'Lam', 'Rarri', 'Honda', 'Toyota', 'Jaguar', 'Tesla'];

//Car class and constructor
class Car {
  constructor(name, speed, tacho = 0) {
    this.speed = speed; // km/h
    this.name = name;
    this.tacho = tacho; // km
  }
  //race method 
  race(time) {
    this.tacho += this.speed * time;
  }
  //logs car info
  printData() {
    console.log(`Car name: ${this.name}`);
    console.log(`Car speed: ${this.speed} km/h`);
    console.log(`This car travelled for: ${time.value} hours`);
    console.log(`Car tacho: ${this.tacho} km`);
    console.log('----------------------');
  }
};

//array of car object 
let cars = [];

addCar.addEventListener('click', function() {
  console.clear();
  //gets speed and name of cars from user input.
  let speed = Number(document.getElementById('speed').value);
  let name = document.getElementById('carMake').value;

  //pushes new object to the array 
  cars.push(new Car(name, speed));

  //shows time and 
  document.getElementById('travelled').classList.remove('hidden');

  cars.forEach(function(car) {
    car.printData();
  });

});

//reveals race button
finish.addEventListener('click', function() {
  raceBtn.classList.remove('hidden');
  time.classList.remove('hidden');
});

//race button functions
raceBtn.addEventListener('click', function() {
  console.clear();
  cars.forEach(function(car) {
    car.race(Number(time.value));

    //shows result div
    results.classList.remove('hidden');

    document.getElementById('done').classList.remove('hidden');

    resultParagraph.innerHTML += `Car make is: ${car.name}. It has speed of: 
    ${car.speed} km/h. In ${time.value} hours it travelled ${car.tacho} km. <br>`;

    car.printData();

    resetDiv.classList.remove('hidden');
  });
});

//randomizing function (from mdn)
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
};

//random car creator
random.addEventListener('click', function(){
  let speed = document.getElementById('speed');
  speed.value = getRandomInt(10, 500);
  let name =  document.getElementById('carMake');
  name.value = carNames[getRandomInt(0, carNames.length - 1)];
});

//reset button
reset.addEventListener('click', function(){
  window.location.reload();
  console.clear();
})