let button = document.getElementById('btn');
let chatBox = document.getElementById('chat');
let input = document.getElementById('input');


function messageSent() {
  if (input.value) {
  let date = new Date();
  input.value = input.value.replace(':)', '😀');
  chatBox.innerHTML += `<br> ${date.getHours()}:${date.getMinutes()} ${input.value}`;
  input.value = '';
  }
}

button.addEventListener('click', messageSent);

input.addEventListener('keypress', function(e){
  if (e.key === 'Enter') {
    messageSent();
  }
  
});