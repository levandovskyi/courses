// let baller = 'Arvydas';
let baller2 = 'Domantas';

let ballers = ['Arvydas', 'Domantas', 'Russel', ['Kevin' , 'Lebron']];

let baller = {
  name: 'Kevin Durant',
  seasons: 10,
  points: 2200,
  teams: ['OKC', 'GSW'],
  shoes: {
    name: 'Nike Tavas SD',
    size: 55,
    price: 160,
  }
};

let zalgiris = [
  {
    name: 'Paulius Jankunas',
    seasons: '15'
  },

  {
    name: 'Edgaras Ulanovas',
    seasons: 7
  }
];

console.log(typeof(ballers));
console.log(ballers[0]);
console.log(ballers[3][0]);

console.log(baller.name + "'s first team is: " + baller.teams[0] + '!');
console.log('Number of teams: '+ baller.teams.length);

console.log(zalgiris[1].seasons);

if (baller.seasons >= 10) {
  console.log(baller.name + ' is a legend!');
} else {
  console.log(baller.name + ' is not there yet...')
}

if (baller.shoes.size === 55) {
  console.log('Shoe size too big!');
}

if (baller.seasons % 2 !=0 ) {
  console.log('Seasons number is not even?')
} else {
  console.log('Seasons number is even')
};

let durantPA = baller.points / baller.seasons;
console.log(baller.name + ' average points per game ' + durantPA.toFixed(0));