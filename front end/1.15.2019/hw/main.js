document.getElementById("btn").addEventListener("click", function(e) {
  e.preventDefault();
  let time = document.getElementById("time").value;
  let distance = document.getElementById("distance").value;
  if (!distance || !time) {
    alert("No funny bussiness!");
    return;
  }

  if (distance <= 0 || time <= 0) {
    alert("we are not playing around.");
    return;
  }

  let cars = [
    {
      name: document.getElementById("model").value,
      number: document.getElementById("plate").value,
      distance: distance,
      time: time
    }
  ];

  console.log(cars);

  cars.forEach(car => {
    let speed = car.distance / car.time;
    let htmlLog = document.getElementById("speedWrite");
    let reaction = document.getElementById("reaction");

    if (speed >= 60) {
      console.log(
        "Car number is: " +
          car.number +
          " cars model is: " +
          car.name +
          " average speed is: " +
          speed.toFixed(2) +
          "km/h. It travelled: " +
          car.distance +
          " km. It took the driver: " +
          car.time +
          " hour(s). Ladies and gentleman...we got him."
      );
      htmlLog.innerHTML =
        "This guys speed was: " +
        speed +
        "km/h! Ticket sent. No questions asked.";
      if (!reaction.classList.contains("reactHap")) {
        reaction.classList.add("reactHap");
      }
    } else {
      console.log(
        "Car number is: " +
          car.number +
          " the model is: " +
          car.name +
          " average speed is: " +
          speed.toFixed(2) +
          "km/h" +
          " the driver is too smooth we will get him next time..."
      );

      if (!reaction.classList.contains("reactSad")) {
        reaction.classList.add("reactSad");
      }

      htmlLog.innerHTML =
        "This cars speed was: " +
        speed.toFixed(2) +
        "km/h..." +
        " The driver is too smooth we will get him next time...";
    }
  });
});

document.getElementById("refresh").addEventListener("click", function() {
  window.location.reload();
});
