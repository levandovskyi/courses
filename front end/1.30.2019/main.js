let obj = {
  name: 'Peter',
  age: 35,
  isMarried: true,
  kids: {
    name: 'John',
    age: 12,
    isMarried: false
  },
  interests: ['football', 'chess', 'guitar']
}

$.ajax({url: 'https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo', success: result => {
  console.log(result);
  $('#title').html(result.title);
  $('#div1').html(result.explanation);
  $('body').css('background-image', `url(${result.hdurl})`);
  $('body').css('background-position', `0 -100px`);
  $('body').css('color', 'white');
}, error: () => {
  alert('Link not loaded!');
}});

let objJSON = JSON.stringify(obj);
let objJSONParse = JSON.parse(objJSON);

console.log(objJSONParse);
console.log(obj);
console.log(objJSON);

