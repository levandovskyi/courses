let buttonPress = new Audio('sound/AK47 Shot CSGO Sound Clip.mp3');
let succ = new Audio('sound/yousuck.mp3');
let keyPress = new Audio('sound/Uh Steve Minecraft.mp3');
let winSound = new Audio('sound/fatality.mp3');

let ui = {
  word: document.getElementById('word'),
  loadingBar: document.getElementById('loadingBar'),
  message: document.getElementById('message'),
  buttonEasy: document.getElementById('easy'),
  buttonMedium: document.getElementById('medium'),
  buttonHard: document.getElementById('hard'),
  menu: document.getElementById('menu'),
  menuBtn: document.getElementById('menuBtn')
};

let game = {
  word: 'bomba',
  level: 1,
  progress: 0,
  intervalID: null,

  //methods
  drawLetters() {
    ui.word.innerHTML = '';
    for (let i = 0; i < this.word.length; i++) {
      ui.word.innerHTML += `<div class="letter"></div>`;
    }
  },
  checkLetter(e) {
    let letterFound = false;
    console.log(e.key);

    for (let i = 0; i < this.word.length; i++) {
      if (this.word[i] === e.key) {
        //letter correct
        console.log('letter correct', i);

        letterFound = true;
        ui.word.children[i].innerHTML = e.key;
      }
    }
    if (!letterFound) {
      this.addProgress(10);
    }

    //if win

    for (let letter of ui.word.children) {
      if (!letter.innerHTML) return;
    }
    ui.message.innerHTML = 'Good job!';
    winSound.play();

    setTimeout(() => {
      this.init();
    }, 2000);
  },
  addProgress(amount) {
    this.progress += amount * this.level;
    this.progress = Math.min(this.progress, 100);
    this.drawProgress();

    if (this.progress === 100) {
      ui.message.innerHTML = 'Try harder next time...';
      //plays succ
      succ.play();
      this.init();
    }
  },
  drawProgress() {
    ui.loadingBar.style.width = this.progress + '%';
  },
  init() {
    let words = ['bomba', 'terrorist', 'allah', 'virgins', 'isis'];

    let randomIndex = Math.floor(Math.random() * words.length);

    this.word = words[randomIndex];
    this.drawLetters();
    this.progress = 0;
    this.drawProgress();
  },
  start() {
    ui.loadingBar.classList.add('hidden');
    ui.message.classList.add('hidden');
    ui.buttonEasy.addEventListener('click', () => {
      this.level = 1;
      ui.loadingBar.classList.remove('hidden');
      ui.message.classList.remove('hidden');
      ui.menu.classList.add('hidden');
      clearInterval(this.intervalID);
      this.intervalID = setInterval(() => {
        game.addProgress(0.5);
      }, 300);
      this.init();
    })
    ui.buttonMedium.addEventListener('click', () => {
      this.level = 2;
      ui.loadingBar.classList.remove('hidden');
      ui.message.classList.remove('hidden');
      ui.menu.classList.add('hidden');
      clearInterval(this.intervalID);
      this.intervalID = setInterval(() => {
        game.addProgress(0.5);
      }, 300);
      this.init();
    })
    ui.buttonHard.addEventListener('click', () => {
      this.level = 5;
      ui.loadingBar.classList.remove('hidden');
      ui.message.classList.remove('hidden');
      ui.menu.classList.add('hidden');
      clearInterval(this.intervalID);
      this.intervalID = setInterval(() => {
        game.addProgress(0.5);
      }, 300);
      this.init();
    })
  }

};

//pausing game
ui.menuBtn.addEventListener('click', () => {
  ui.menu.classList.toggle('hidden');
});

//starting game
game.start();

document.addEventListener('keyup', e => {
  game.checkLetter(e);
});

document.addEventListener('keypress', () => {
  keyPress.play();
});

ui.buttonEasy.addEventListener('click', () => {
  buttonPress.play();
});

ui.buttonMedium.addEventListener('click', () => {
  buttonPress.play();
});

ui.buttonHard.addEventListener('click', () => {
  buttonPress.play();
});
