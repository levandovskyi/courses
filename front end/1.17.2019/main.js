function sayHello(name, isAmerican) {
  if(isAmerican) {
  console.log(`Hello ${name}!`);
  } else {
    console.log(`Labas, ${name}!`);
  }
  
}

sayHello('Rytis', false);
sayHello('John', true);
sayHello('Donald', true);

let triangles = [
  {
    a: 1,
    b: 3,
    c: 8
  },
  {
    a: 1,
    b: 3,
    c: 7
  },
  {
    a: 2,
    b: 2,
    c: 8
  },
]



function trianglePerimeter(a, b, c) {
  let p = a + b + c;

  return p;
}

function isEven(a, b, c) {
  let even = trianglePerimeter(a, b, c) % 2;
  if (even === 0) {
    return true;
  } else {
    return false;
  }
}





triangles.forEach(function (triangle) {
  let p = trianglePerimeter(triangle.a, triangle.b, triangle.c);
  let even = isEven(triangle.a, triangle.b, triangle.c);
  if (even == true) {
    console.log('Triangles perimeter is even')
  } else {
    console.log ('Triangle perimeter is not even')
  }
});


// for (let i = 0; i < triangles.length; i++) {
//   let p = trianglePerimeter(triangles[i].a, triangles[i].b, triangles[i].c);
//   console.log(p);
// }

// let i = 0;

// while (i < triangles.length) {
//   let p = trianglePerimeter(triangles[i].a, triangles[i].b, triangles[i].c)
//   console.log(p)
//   i++;
// }