let rectangles = [
  {
    width: 10,
    height: 10
  },
  {
    width: 20,
    height: 15
  },
  {
    width: 120,
    height: 30
  }
]

let areas = [];


function perimeter(a, b) {
  return a + b;
}


function area(a, b) {
  return a * b;
}


function diagonal(a, b) {
  return Math.sqrt(Math.pow(a, 2) + Math.pow(a, 2));
}

function biggest(a) {
  return Math.max(...a);
};

function big(a) {
  return a.indexOf(biggest(a));
}


rectangles.forEach(function(rectangle, index) {
  let perim = perimeter(rectangle.height, rectangle.width);
  let are = area(rectangle.width, rectangle.height);
  let diag = diagonal(rectangle.width, rectangle.height);

  areas.push(are);

  console.log('Rectangle nr: ' + index + ' info: perimeter: ' + perim + ' area: ' + are + ' diagonal: ' + diag);

})



console.log('Rectangle nr: ' + big(areas) + ' is the biggest. Its area is: ' + biggest(areas) + ' cm.' );
