// let rectangles = [
//   {
//     width: 150,
//     height: 90
//   },
//   {
//     width: 500,
//     height: 70
//   },
//   {
//     width: 90,
//     height: 150
//   },
//   {
//     width: -150,
//     height: 90
//   }
// ];

let rectangles = [];

document.getElementById('btn').addEventListener('click', function (e) {
  e.preventDefault();
  console.clear();

  let width = document.getElementById('width').value;
  let height = document.getElementById('height').value;

  if (!width || !height) {
    alert('enter the values.')
    return
  }

  rectangles.push({
    width: width,
    height: height
  })

  rectangles.forEach(function (rectangle, index) {
    let logHtml = document.getElementById('info');
    let perimeter = rectangle.width * 2 + rectangle.height * 2;
    let area = rectangle.width * rectangle.height;
    let diagonal = Math.sqrt(
      Math.pow(rectangle.height, 2) + Math.pow(rectangle.width, 2)
    );

    if (rectangle.width > 0 && rectangle.height > 0) {
      console.log(
        "Rectangle nr: " +
        index + ' width is: ' + 
        rectangle.width + "cm. It's height is: " + rectangle.height +
        "cm. Perimeter value is: " +
        perimeter +
        "cm. It has area of: " +
        area +
        "sqrcm, and diagonal: " +
        diagonal + "cm."
      );
      logHtml.innerHTML = 'Congratulations! It does!'

    } else {
      console.log("Rectangle nr.: " + index + ' has width of: ' + rectangle.width + 'cm and height of: ' + rectangle.height + "cm. Therefore this rectangle can not exist :(");
      logHtml.innerHTML = 'I am verry sorry.. It does not...'
    }
  });
});

// reset button

document.getElementById('reset').addEventListener('click', function () {
  window.location.reload();
})
