class Pokemon {
    constructor(name, hp, attack, defense, level, states) {
        this.name = name;
        this.hp = hp;
        this.totalHp = hp;
        this.attack = attack;
        this.defense = defense;
        this.level = level;
        this.states = states;
    }

    //prints pokemon stats
    printData() {
        console.log("--Pokemon info--");
        console.log(`Name: ${this.name}`);
        console.log(`Attack: ${this.attack}`);
        console.log(`Defense: ${this.defense}`);
        console.log(`HP: ${this.hp}`);
        console.log(`Level: ${this.level}`);
        console.log(`---------------`);
    }

    //add hp fuction
    addHp(value) {
        this.hp += value;
    }

    //checks power level
    checkPower(enemy) {
        if (this.attack > enemy.attack) {
            console.log(`${this.name} is stronger than ${enemy.name}`);
        } else {
            console.log(`${this.name} is weaker than ${enemy.name}`);
        }
    }

    //transforms hp to percents
    getPercHp() {
        return Math.max(0, Math.round(100 * this.hp / this.totalHp));
    }

    //fight function
    fight(enemy) {

        let enemyAttackCalc = Math.min(Math.round((enemy.attack * enemy.level / this.defense) * Math.random() + Math.random() * (this.defense * Math.random())), 10);
        let thisAttackCalc = Math.min(Math.round((this.attack * this.level / enemy.defense) * Math.random() + Math.random() * (enemy.defense * Math.random())), 10);

        this.hp -= enemyAttackCalc;
        let thisTotal = this.getPercHp();
        hpBarFirst.style.width = thisTotal + '%';
        this.hp = Math.max(0, this.hp);

        console.log(
            `${enemy.name} hit ${this.name} with power of ${enemyAttackCalc}. ${
                this.name
                } has ${this.hp.toFixed(1)}HP left.`
        );
        console.log("-------------------");

        enemy.hp -= thisAttackCalc;
        let enemyTotal = enemy.getPercHp();
        hpBarSecond.style.width = enemyTotal + '%';
        enemy.hp = Math.max(0, enemy.hp);

        console.log(
            `${this.name} hit ${enemy.name} with power of ${thisAttackCalc}. ${
                enemy.name
                } has ${enemy.hp.toFixed(1)}HP left.`
        );
        console.log("-------------------");
    }
}
