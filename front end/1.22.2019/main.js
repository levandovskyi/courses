// list of variables
let selectFirst = document.getElementById('select1');
let selectSecond = document.getElementById('select2');
let fighterFirst = document.getElementById('fighterFirst');
let fighterSecond = document.getElementById('fighterSecond');
let idleFirst = document.getElementById('idleFirst');
let idleSecond = document.getElementById('idleSecond');
let fightBtn = document.getElementById('btn');
let imageFirst = document.getElementById('imageFirst');
let imageSecond = document.getElementById('imageSecond');
let hpBarFirst = document.getElementById('healthFirst');
let hpBarSecond = document.getElementById('healthSecond');
let won = document.getElementById('won');

//global fighter variables
let fighter1;
let fighter2;

//function to get each array element idex to act as select option value
pokemons.forEach(function(pokemon, index) {
  selectFirst.innerHTML += `<option value="${index}">${pokemon.name}</option>`;
  selectSecond.innerHTML += `<option value = "${index}">${
    pokemon.name
  }</option>`;
});

//function to create left pokemon from the pokemon array
selectFirst.addEventListener('change', function(e) {
  fighterFirst.classList.remove('notExist');
  let value = [Number(e.target.value)];
  imageFirst.src = pokemons[value].states.idle;
  fighter1 = new Pokemon(
    pokemons[value].name,
    pokemons[value].hp,
    pokemons[value].attack,
    pokemons[value].defense,
    pokemons[value].level,
    pokemons[value].states
  );
  fighter1.printData();
});

//function to create right pokemon from the pokemon array
selectSecond.addEventListener('change', function(e) {
  fighterSecond.classList.remove('notExist');
  let value = [Number(e.target.value)];
  imageSecond.src = pokemons[value].states.idle;
  fighter2 = new Pokemon(
    pokemons[value].name,
    pokemons[value].hp,
    pokemons[value].attack,
    pokemons[value].defense,
    pokemons[value].level,
    pokemons[value].states
  );
  console.clear();
  fighter1.printData();
  fighter2.printData();
});

//animation adding function left
function fightFirst(a) {
  a.style.animation = 'fightFirst';
  a.style.animationDuration = 1500 + 'ms';
  a.style.animationIterationCount = 'infinite';
}

//anitmation adding function right
function fightSecond(a) {
  a.style.animation = 'fightSecond';
  a.style.animationDuration = 1500 + 'ms';
  a.style.animationIterationCount = 'infinite';
}

//after pressing the button the fight will start, fighter objects will change their state.
fightBtn.addEventListener('click', function() {
  if (fighter1 !== undefined && fighter2 !== undefined) {
    imageFirst.src = fighter1.states.fight;
    imageSecond.src = fighter2.states.fight;

    fightProcess();
    fighterFirst.classList.remove('idleFirst');
    fightFirst(fighterFirst);
    fighterSecond.classList.remove('idleSecond');
    fightSecond(fighterSecond);
  } else {
    alert('You must pick a fighter!');
  }
});

//fight function with timeout set to 3s
function fightProcess() {
  setTimeout(function() {
    fighter1.fight(fighter2);

    //they will fight till one of them has 0 hp.
    if (fighter1.hp > 0 && fighter2.hp > 0) {
      fightProcess();
    } else if (fighter1.hp == 0) {
      //state change after left fighter lose
      console.log(`${fighter1.name} got beaten up!`);
      won.innerHTML = `${fighter2.name} WON!!!`;
      imageFirst.style.filter = fighter1.states.dead;
      imageSecond.src = fighter2.states.idle;
      fighterFirst.style.animation = 'none';
      fighterSecond.style.animation = 'none';
    } else if (fighter2.hp == 0) {
      //state change after right fighter lose
      console.log(`${fighter2.name} got beaten up!`);
      won.innerHTML = `${fighter1.name} WON!!!`;
      imageFirst.src = fighter1.states.idle;
      imageSecond.style.filter = fighter2.states.dead;
      fighterFirst.style.animation = 'none';
      fighterSecond.style.animation = 'none';
    }
  }, 1500);
}

//reset button (reloads page)
document.getElementById('reset').addEventListener('click', function() {
  window.location.reload();
});
