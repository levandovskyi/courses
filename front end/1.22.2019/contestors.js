let pokemons = [
  {
    name: 'Pikachu',
    hp: 35,
    attack: 55,
    defense: 30,
    level: 1,
    states: {
    idle: 'img/Pikachu.png',
    fight: 'img/Angry-Pikachu-Transparent-PNG.png',
    dead: 'grayscale(100%)'}
  },
  {
    name: 'Squirtle',
    hp: 44,
    attack: 48,
    defense: 60,
    level: 1,
    states: {
    idle: 'img/580b57fcd9996e24bc43c32a.png',
    fight: 'img/007Squirtle_OS_anime_3.png',
    dead: 'grayscale(100%)'}
  },
  {
    name: 'MewTwo',
    hp: 106,
    attack: 110,
    defense: 90,
    level: 1,
    states: {
    idle: 'img/mewtwo.png',
    fight: 'img/150-Mewtwo.png',
    dead: 'grayscale(100%)'}
  },
];