class Product {
  constructor(name, price, quantity, category) {
    this.name = name;
    this.price = price;
    this.quantity = quantity;
    this.category = category;
  }

  printData() {
    console.log(`name: ${this.name}`);
    console.log(`price: ${this.price} $`);
    console.log(`quantity: ${this.quantity} vnt.`);
    console.log(`category: ${this.category}.`);
  }

  addQuantity(amount) {
    this.quantity += amount;
  }

  total(){
    return this.quantity * this.price;
  }
}

function printProducts() {
  console.clear();
  for (let product of products) {
    product.printData();
  }
};

let products = [];

let button = document.getElementById('addBtn');

let nameIn = document.getElementById('nameInput');
let priceIn = document.getElementById('priceInput');
let quantityIn = document.getElementById('quantityInput');
let select = document.getElementById('categoryIput');
let table = document.getElementById('table');

button.addEventListener('click', function(){
  let product = new Product(nameIn.value, priceIn.value, quantityIn.value, select.value);

  products.push(product);

  printProducts();
  drawProducts();
});

function drawProducts() {
  table.innerHTML = '';

  for (let product of products) {
    table.innerHTML += 
    `<tr>
          <td>${product.name}</td>
          <td>${product.price} $ </td>
          <td>${product.quantity}</td>
          <td>${product.category}</td>
          <td>${product.total()}</td>
    </tr>`;
  }
}


