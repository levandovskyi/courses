let triangles = [
  {
    a: 4,
    b: 3,
    c: 5
  },
  {
    a: 1,
    b: 3,
    c: 7
  },
  {
    a: 2,
    b: 2,
    c: 8
  },
]



function trianglePerimeter(a, b, c) {
  let p = a + b + c;

  return p;
}

function isEven(a, b, c) {
  let even = trianglePerimeter(a, b, c) % 2;
  if (even === 0) {
    return true;
  } else {
    return false;
  }
}

function exists(a, b, c) {
  if (a + b > c && a + c > b && c + b > a) {
    return true;
  } else {
    return false;
  }
}

function isRight(a, b, c) {
  return a * a + b * b == c * c || b * b + c * c == a * a || a * a + c * c == b * b;
}


triangles.forEach(function (triangle) {
  if (exists(triangle.a, triangle.b, triangle.c)) {
    console.log('triangle exists')
  } else {
    console.log('triangle doesnt exist')
  }

  if (isRight(triangle.a, triangle.b, triangle.c)){
    console.log('triangle is right')
  } else {
    console.log('triangle is not right')
  }
})


triangles.forEach(function (triangle) {
  let p = trianglePerimeter(triangle.a, triangle.b, triangle.c);
  let even = isEven(triangle.a, triangle.b, triangle.c);
  if (even == true) {
    console.log('Triangles perimeter is even')
  } else {
    console.log ('Triangle perimeter is not even')
  }
});


let planet = {
  name: 'Earth',
  radius: 6371,
  hasLife: true,
  printData() {
    console.log(`name: ${this.name}`);
    console.log(`radius: ${this.radius}`);
    console.log(`Does this planet has life: ${this.hasLife ? 'yes' : 'no'} `);
  }
};

planet.printData();
