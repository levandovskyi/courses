package com.levandovskyi.snake;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.time.*;

public class Frame {
    static GraphicsConfiguration g;
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(jFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(500,500);
        jFrame.setResizable(false);
        int rows = 10;
        int cols = 10;
        JPanel board = new JPanel(new GridLayout(rows , cols));
        board.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        for(int i=0; i < (rows * cols); i++) {
            final JLabel label = new JLabel("label");
            label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            board.add(label);
        }
        jFrame.add(board);
    }
}
