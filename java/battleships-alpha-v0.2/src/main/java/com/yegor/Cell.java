package com.yegor;

public class Cell {
    private boolean ship;
    private boolean hit;
    private boolean miss;
    private boolean shipArea;
    private int row;
    private int col;

    public Cell() {
        this.ship = false;
        this.hit = false;
        this.miss = false;
        this.shipArea = false;
    }

    public Cell(int row, int col) {
        this.row = row;
        this.col = col;
    }


    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public boolean isShip() {
        return ship;
    }

    public void setShip(boolean ship) {
        this.ship = ship;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public boolean isMiss() {
        return miss;
    }

    public void setMiss(boolean miss) {
        this.miss = miss;
    }

    public boolean isShipArea() {
        return shipArea;
    }

    public void setShipArea(boolean shipArea) {
        this.shipArea = shipArea;
    }
}
