package com.yegor;

public interface IBoard {

    void draw();
    boolean addShipHorizontal(int col, int row, int size);
    boolean addShipVertical(int col, int row, int size);
    void attack(int col, int row);

}
