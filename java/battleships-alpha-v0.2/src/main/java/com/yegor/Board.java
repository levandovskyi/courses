package com.yegor;

import java.util.ArrayList;
import java.util.Random;

public class Board implements IBoard {
    private final int rows = 10;
    private final int cols = 10;
    private final Cell[][] board = new Cell[rows][cols];
    private ArrayList<ArrayList<Integer[]>> shipList = new ArrayList<>();
    private ArrayList<Integer[]> shipPartList = new ArrayList<>();

    public Board() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                this.board[i][j] = new Cell();
            }
        }
    }

    @Override
    public void draw() {
        System.out.println("  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |");
        System.out.println("--+---+---+---+---+---+---+---+---+---+---|");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < board.length; i++) {
            sb.delete(0, sb.length());
            sb.append(i + " |");
            for (int j = 0; j < board[i].length; j++) {
                if (this.board[i][j].isHit()) {
                    sb.append("\033[31;1m X\033[0m |");
                } else if (this.board[i][j].isShip()) {
                    sb.append("\u001B[32m O \033[0m|");
                } else if (this.board[i][j].isMiss()) {
                    sb.append(" * |");
                } /* else if(this.board[i][j].isShipArea()) {
                    sb.append(" # |");
                } */else {
                    sb.append(" \033[34m~ \033[0m|");
                }
            }
            System.out.println(sb.toString());
            System.out.println("--+---+---+---+---+---+---+---+---+---+---|");
        }
    }

    @Override
    public boolean addShipVertical(int row, int col, int size) {
        if (row + size >= rows) {
            //System.out.println("Ship cant fit!");
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (board[row + i][col].isShip() || board[row + i][col].isShipArea()) {
                board[row + i][col].setShipArea(false);
                return false;
            }

            if (i == 0) {
                if (row - 1 >= 0) {
                    board[row - 1][col].setShipArea(true);

                    if (col - 1 >= 0) {
                        board[row - 1][col - 1].setShipArea(true);
                    }

                    if (col + 1 < cols) {
                        board[row - 1][col + 1].setShipArea(true);
                    }
                }

            }

            if (col - 1 >= 0) {
                board[row + i][col - 1].setShipArea(true);
            }

            if (col + 1 < cols) {
                board[row + i][col + 1].setShipArea(true);
            }

            if (i == size - 1) {
                if (row + i + 1 < rows) {
                    board[row + i + 1][col].setShipArea(true);

                    if (col - 1 >= 0) {
                        board[row + i + 1][col - 1].setShipArea(true);
                    }

                    if (col + 1 < cols) {
                        board[row + i + 1][col + 1].setShipArea(true);
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean addShipHorizontal(int row, int col, int size) {
        if (col + size >= cols) {
            return false;
        }

        for (int i = 0; i < size; i++) {
            if (board[row][col + i].isShip() || board[row][col + i].isShipArea()) {
                return false;
            }

            if (i == 0) {
                if (col - 1 >= 0) {
                    board[row][col - 1].setShipArea(true);

                    if (row - 1 >= 0) {
                        board[row - 1][col - 1].setShipArea(true);
                    }
                    if (row + 1 < rows) {
                        board[row + 1][col - 1].setShipArea(true);
                    }
                }
            }
            if (row - 1 >= 0) {
                board[row - 1][col + i].setShipArea(true);
            }
            if (row + 1 < rows) {
                board[row + 1][col + i].setShipArea(true);
            }

            if (i == size - 1) {
                if (col + i + 1 < cols) {
                    board[row][col + i + 1].setShipArea(true);
                    if (row - 1 >= 0) {
                        board[row - 1][col + i + 1].setShipArea(true);
                    }
                    if (row + 1 < rows) {
                        board[row + 1][col + i + 1].setShipArea(true);
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void attack(int row, int col) {
        if (this.board[row][col].isShip()) {
            this.board[row][col].setHit(true);
            shipPartList.remove(shipPartList.size() - 1);
            if (shipPartList.size() == 0){
                shipList.remove(shipList.size() - 1);
            };
        } else {
            this.board[row][col].setMiss(true);
        }
    }

    private void autoPosition(int size) {
        boolean a = false;
        while (!a) {
            a = fits(size);
        }
    }


    /**
     * @param size - size of the ship.
     * @return - returns true if ship can fit on a board.
     */


    public boolean fits(int size) {
        Random r = new Random();
        int row = r.nextInt(rows);
        int col = r.nextInt(cols);
        boolean vertical = r.nextBoolean();

        final boolean b = false;
        if (row < 0 || col < 0) return b;
        if (vertical) {
            if (addShipVertical(row, col, size)) {
                for(int i = 0; i < size; i++) {
                    board[row + i][col].setShip(true);
                    this.board[row + i][col].setRow(row);
                    this.board[row + i][col].setCol(col);
                }
                shipPartList.add(new Integer[] {row, col});
                shipList.add(shipPartList);
                return true;
            }
            return b;
        } else {
                if (addShipHorizontal(row, col, size)) {
                    for(int i = 0; i < size; i++) {
                        board[row][col + i].setShip(true);
                        this.board[row][col].setRow(row);
                        this.board[row][col].setCol(col);
                        shipPartList.add(new Integer[] {row, col});
                    }
                    shipList.add(shipPartList);
                    return true;
                }
        }
        return b;
    }

    public void enemyShips() {
        autoPosition(5);
        autoPosition(4);
        autoPosition(3);
        autoPosition(3);
        autoPosition(2);
    }

    public int enemyShipsSize() {
        return shipList.get(1).get(0).length;
    }

}
