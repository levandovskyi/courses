package com.cars;

public class Benz extends Car {
    String model;

    public Benz(String engine, int power, String pollution, String name, String steering, String drive, int doors, int speed, int gears, String model) {
        super(engine, power, pollution, name, steering, drive, doors, speed, gears);
        this.model = model;
    }
}
