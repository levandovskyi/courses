package com.cars;

public class Vehicle {
    String engine;
    int power;
    String pollution;
    int currentSpeed = 0;

    public Vehicle(String engine, int power, String pollution) {
        this.engine = engine;
        this.power = power;
        this.pollution = pollution;
    }
}
