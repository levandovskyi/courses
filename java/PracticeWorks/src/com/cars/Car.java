package com.cars;

public class Car extends Vehicle {
    String name;
    String steering;
    String drive;
    int doors;
    int speed;
    int gears;

    public Car(String engine, int power, String pollution, String name, String steering, String drive, int doors, int speed, int gears) {
        super(engine, power, pollution);
        this.name = name;
        this.steering = steering;
        this.drive = drive;
        this.doors = doors;
        this.speed = speed;
        this.gears = gears;
    }

    public void increaseSpeed(int a) {
        if(this.currentSpeed > this.speed) {
            System.out.println("Invalid speed");
        } else {
            this.currentSpeed +=  a;
        }
    }

    public void decreaseSpeed(int a) {
        this.speed -= a;
    }

    public void shiftUp(int a) {
        if(a > this.gears) {
            System.out.println("Invalid shift");
        } else {
            this.gears += a;
        }
    }

    public void shiftDown(int a) {
        if(a < 1) {
            System.out.println("Invalid Shift");
        } else {
            this.gears -= a;
        }
    }
}
