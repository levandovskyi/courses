package com.house;

public class Main {
    public static void main(String[] args) {
        Furniture[] furniture = {
                new Sofa(500, "premium leather", 3, "dark brown"),
                new Chair(200, "wood","suede")
        };
        House room = new House(new Door("brown", "wood"), new Wall("yellow"), furniture);

        room.getInfo();
    }
}
