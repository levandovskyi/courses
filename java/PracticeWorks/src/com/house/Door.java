package com.house;

public class Door {
    private String color;
    private String material;

    public Door(String color, String material) {
        this.color = color;
        this.material = material;
    }

    public String prinItnfo() {
        return this.color + " " + this.material;
    }
}
