package com.house;

public class Chair extends Furniture {
    private String baseMaterial;
    private String seatMaterial;

    public Chair(int price, String baseMaterial, String seatMaterial) {
        super(price, "Chair");
        this.baseMaterial = baseMaterial;
        this.seatMaterial = seatMaterial;
    }
}
