package com.house;

public class Furniture {
    private int price;
    private String type;

    public Furniture(int price, String type) {
        this.price = price;
        this.type = type;
    }
}
