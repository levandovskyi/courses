package com.house;

public class House {
    private Door door;
    private Wall walls;
    private Furniture[] furniture;

    public House(Door door, Wall walls, Furniture[] furniture) {
        this.door = door;
        this.walls = walls;
        this.furniture = furniture;
    }

    public void getInfo() {
        System.out.println(this.door.printInfo());
    }
}
