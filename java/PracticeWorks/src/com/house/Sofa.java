package com.house;

public class Sofa extends Furniture {
    private String material;
    private int space;
    private String color;

    public Sofa(int price, String material, int space, String color) {
        super(price, "Sofa");
        this.material = material;
        this.space = space;
        this.color = color;
    }
}
