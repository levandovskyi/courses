package com.banking;

public class Account {
    private String name;
    private int id;
    private int balance;

    public Account(String name, int id, int balance) {
        this.name = name;
        this.id = id;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void addFunds(int amount) {
        this.balance += amount;
    }

    public void reduceFunds(int amount) {
        if(this.balance - amount < 0) {
            System.out.println("Not enough funds!");
            return;
        } else {
            this.balance -= amount;
            System.out.println("Transaction completed! Account balance: " + this.balance);
        }
    }
}
