package com.banking;

public class Main {
    public static void main(String[] args) {
        Account a = new Account("John Wick", 458_254_471, 0);
        a.addFunds(25);
        a.reduceFunds(25);
    }
}
