package com.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Collection;

public class Main {
    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect("https://jsoup.org/apidocs/overview-summary.html").get();
        Elements titles = doc.getElementsByClass("contentContainer");
        for (Element title : titles) {
            System.out.println(title.text());
        }
    }
}
