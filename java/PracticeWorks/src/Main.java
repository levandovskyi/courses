import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Time clock = new Time();
        int sum = 0;
        int count = 0;
        for(int i=1; i <= 1000; i++) {
            if(i % 3 == 0 && i % 5 == 0) {
//                sum += i;
                count++;
//                System.out.println(i);
                if(count == 5) break;
            }
        }
//        System.out.println(sum);

        int number = 4;
        int finish = 40;
        int total = 0;
        while(number <= finish) {
            number++;
            if(!isEven(number)) {
                continue;
            }
//            System.out.println(number);

            total++;
            if(total >= 5) {
                break;
            }
        }
//        System.out.println("Found a total of " + total + " even numbers");
        int n = 9;

        if(sumOfDigits(n) != -1) {
//            System.out.println(sumOfDigits(n));
        } else {
//            System.out.println("invalid");
        }


        int[] arr = new int[10];
        for(int i=0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
            sum += arr[i];
            System.out.println(sum);
        }



    }

    public static boolean isEven(int n) {
        if(n % 2 == 0) {
            return true;
        }
        return false;
    }

    public static int sumOfDigits(int n) {
        if(n < 10) {
            return -1;
        } else if (n / 10 == 0) {
            return -1;
        }
        int saveLast = n % 10;
        int deleteLast = n / 10;
        int saveLastSecond = deleteLast % 10;
        int deleteLastSecond = deleteLast / 10;
        int saveLastThird = deleteLastSecond % 10;

        return saveLastThird + saveLastSecond + saveLast;
    }
}

class Time {
    public String calc(long minutes, long seconds) {
        if(minutes < 0 || seconds > 59)
            return "Invalid value";

        long hours = minutes / 60;
        long remaining = minutes % 60;

        String newHours = hours + "h";
        String newRemaining = remaining + "m";

        if(hours < 10)
            newHours = "0" + newHours;
        else if(minutes < 10)
            newRemaining = "0" + newRemaining;
        return newHours + newRemaining + seconds + "s";
    }

    public String calc(long seconds) {
        long minutes = seconds / 60;
        long remainingSeconds = seconds % 60;
        return calc(minutes, remainingSeconds);
    }

}





