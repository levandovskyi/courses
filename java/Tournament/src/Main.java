public class Main {
    public static void main(String[] args) {
        Student[] all = {
                new Student("Bob", "The Builder", 5, new int[]{6, 8, 7, 4, 8}),
                new Student("Francis", "Of The Filth", 5, new int[]{10, 10, 10, 10, 10}),
                new Student("Metal Face", "Doom", 6, new int[]{10, 8, 7, 10, 9}),
                new Student("Peter", "Parker", 4, new int[]{10, 10, 10, 10, 10}),
                new Student("Liquid", "Snake", 6, new int[]{8, 7, 8, 7, 9}),
                new Student("Harry", "Osborn", 4, new int[]{5, 5, 7, 4, 3}),
                new Student("Doctor", "Manhattan", 6, new int[]{10, 10, 10, 10, 10}),
                new Student("Harry", "Potter", 5, new int[]{7, 7, 7, 7, 7}),
                new Student("Colin", "McRae", 4, new int[]{2, 1, 7, 4, 5})
        };

        Calc sort = new Calc();
        sort.sort(all);
        for (int i = 0; i < all.length; i++) {
            System.out.println("Class nr: " + all[i].classNr + "; Name: " + all[i].name + "; Surname: " + all[i].surname +
                    "; Average: " + all[i].calculateAverage());
        }
        System.out.println("");
        System.out.println();
    }
}


class Student {
    String name;
    String surname;
    int classNr;
    int[] marks;
    double avg;

    public Student(String name, String surname, int classNr, int[] marks) {
        this.name = name;
        this.surname = surname;
        this.classNr = classNr;
        this.marks = marks;
    }

    public int compareTo(Student student) {
        this.avg = this.calculateAverage();
        student.avg = student.calculateAverage();

        if (this.classNr > student.classNr) {
            return 1;
        } else if (this.classNr < student.classNr) {
            return -1;
        }

        if (this.avg > student.avg) {
            return -1;
        } else if (this.avg < student.avg) {
            return 1;
        }

        return 0;
    }

    public double calculateAverage() {

        int n = this.marks.length;
        double sum = 0;

        for (int i = 0; i < n; i++) {
            sum += this.marks[i];
        }
        return sum / n;
    }
}


class Calc {

    void sort(Student[] arr) {
        int n = arr.length;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    Student save = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = save;
                }
            }
        }
    }

}