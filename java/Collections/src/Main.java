import java.util.*;

public class Main {
    public static void main(String[] args) {
        Person[] all = {
                new Person("Kobe", 254872321),
                new Person("James", 587412377),
                new Person("Robert", 587412377)
        };

        TreeMap<Integer, String> nameList = new TreeMap<>();
        for(int i=0; i < all.length; i++) {
           String wasChanged = nameList.put(all[i].getId(), all[i].getName());
           if(wasChanged != null) {
               System.out.println("Changed id: " + all[i].getId() + "; Changed name: " + wasChanged + "; New name: " + all[i].getName() + ";");
           } else {
               System.out.println("Added id: " + all[i].getId() + "; Name: " + all[i].getName());
           }
        }
        System.out.println();
        for(Map.Entry<Integer, String> entry : nameList.entrySet()) {
            Integer code = entry.getKey();
            String name = entry.getValue();
            System.out.println(code + " " + name);
        }

    }
}
