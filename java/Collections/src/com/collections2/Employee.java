package com.collections2;

public class Employee {
    private String name;
    private Address address;

    public Employee(String name, String city, String street, String number) {
        this.name = name;
        this.address = new Address(city, street, number);
    }

    public String getName() {
        return name;
    }


    public String getFullAddress() {
        return address.city + " " + address.street + " " + address.number;
    }

    public String getCity() {
        return address.city;
    }

    class Address {
        String city;
        String street;
        String number;

        public Address(String city, String street, String number) {
            this.city = city;
            this.street = street;
            this.number = number;
        }
     }
}
