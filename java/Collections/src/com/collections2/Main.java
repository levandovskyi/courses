package com.collections2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {


        Employee[] office = {
                new Employee("Boris", "Washington", "Washington st.", "52-1"),
                new Employee("Katerina", "Kyiv", "Peter Poroh st.", "32-7"),
                new Employee("John", "Washington", "Freedom st.", "123-4"),
                new Employee("Bob", "New York", "Broadway st", "25"),
                new Employee("Dan", "New York", "Madison st", "1")
        };

        TreeMap<String, String> list = new TreeMap<>();

        for(int i=0; i < office.length; i++) {
            list.put(office[i].getCity(), office[i].getName());
        }

        System.out.println("There are " + list.size() + " different cities. List: ");
        for (Map.Entry<String, String> entry : list.entrySet()) {
            String city = entry.getKey();
            String name = entry.getValue();
            System.out.println(city);
        }

        Collection<Employee> list2 = new ArrayList<>();

    }
}
