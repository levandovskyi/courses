public class Main {
    public static void main(String[] args) {
        //
        Student student1 = new Student("Anthony", "Goodman", 6);
        Student student2 = new Student("Hank", "Green", 10);
        Student student3 = new Student("John", "Jonaitis", 5);
        Student student4 = new Student("Petras", "Hill", 4);
        Student student5 = new Student("Bobby", "Hill", 4);
        Student student6 = new Student("Hank", "Armstrong", 10);
        Student student7 = new Student("Buddy", "Jonaitis", 5);


        Student[] allStudents = {student1, student2, student3, student4, student5, student6, student7};


        Bubble sort = new Bubble();

        sort.sort(allStudents);
        sort.print(allStudents);

    }
}

class Student {
    String name;
    String surname;
    int classNr;

    Student(String name, String surname, int classNr) {
        this.name = name;
        this.surname = surname;
        this.classNr = classNr;
    }

}

class Bubble {
    void sort(Student[] arr) {
        int n = arr.length;

        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-i-1; j++) {
                if (arr[j].classNr > arr[j + 1].classNr) {

                    int save = arr[j].classNr;
                    String saveName = arr[j].name;
                    String saveSurname = arr[j].surname;

                    arr[j].classNr = arr[j + 1].classNr;
                    arr[j + 1].classNr = save;

                    arr[j].name = arr[j + 1].name;
                    arr[j + 1].name = saveName;

                    arr[j].surname = arr[j + 1].surname;
                    arr[j + 1].surname = saveSurname;

                } else if (arr[j].classNr == arr[j + 1].classNr) {
                    if(arr[j].surname.compareTo(arr[i].surname) < 0) {
                        String saveName = arr[j].name;
                        arr[j].name = arr[j+1].name;
                        arr[j+1].name = saveName;
                    } else if(arr[j].surname.compareTo(arr[i].surname) >= 0) {
                        String saveSurname = arr[j].surname;
                        arr[j].surname = arr[j + 1].surname;
                        arr[j+1].surname = saveSurname;
                    }
                }
            }
        }
    }


    void print(Student[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            System.out.println("Class nr: " + arr[i].classNr + "; Surname: " + arr[i].surname + "; Name: " + arr[i].name + ";");
        }
        System.out.println();
    }
}