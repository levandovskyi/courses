class Node {
    String name;
    int value;
    int ID;

    Node left;
    Node right;

    public Node(int value) {

        this.value = value;
        left = null;
        right = null;
    }
}


public class Tree {
    Node root;
    int[] counter;

    Tree(int value, String name, int ID) {
        root = new Node(value);
    }

    public int brackets(int competitors) {
        int brackets = 0;
        for (int i = 2; i <= competitors; i *= 2) {
            brackets = i;
        }
        return brackets;
    }

    public Node levelOrder(Student competitors[], Node root, int level) {
        if(level < competitors.length) {
            Node temp = new Node(competitors[level].getID());
            root = temp;
            root.name = competitors[level].name;

            root.left = levelOrder(competitors, root.left, 2*level + 1);
            root.right = levelOrder(competitors, root.right, 2*level + 2);
        }
        return root;
    }

    public static void inOrder(Node root) {
        if(root != null) {
            inOrder(root.left);
            System.out.print(root.value + " ");
            System.out.println(root.name);
            inOrder(root.right);
        }
    }
}