public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree(0, "Final", 0);
        int allStudents = tree.brackets(20);
        int[] test = {1,2,3,4,5,6,7};
        Student[] competitors = {
                new Student("Winner"),
                new Student("Elon"),
                new Student("James"),
                new Student("John"),
                new Student("Dante")
        };
        for(int i=0; i < competitors.length; i++) {
            competitors[i].setID(i);
        }
        tree.root = tree.levelOrder(competitors, tree.root, 0);
        tree.inOrder(tree.root);
    }
}

class Student {
    String name;
    int ID;

    Student(String name) {
        this.name = name;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }
}

