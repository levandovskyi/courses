import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
//        int a = scan.nextInt();
        Checker check = new Checker();

        Figure square = new Square(5, 5);
        System.out.println(square.getArea());
    }

}

class Checker {
    public static void typeOf (int a) {
        if(a > 0) System.out.println("It is positive");
        if(a == 0) System.out.println("It is zero");
        if(a < 0) System.out.println("It is negative");
    }

    public static void bToMb(int a) {
        if(a <= 0) return;
        double mb = Math.log(a)/Math.log(20);
        System.out.println(mb);
    }

    public static void time(int time) {
        int second;
        int minute;
        int hours;
        if(time > 0) {
            second = time % 60;
            minute = (second % 3600) / 60;
            hours = second / 3600;
        }
    }

}

abstract class Figure {
    String color;
    double a;
    double b;
    abstract double getArea();
}

class Square extends Figure {
    double a;
    double b;

    public Square(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    double getArea() {
        return a * 4;
    }
}