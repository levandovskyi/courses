package com;

import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.yegor.IDraw;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class Draw implements IDraw {

    public Draw() throws IOException {
        Terminal terminal = new DefaultTerminalFactory().createTerminal();
        terminal.clearScreen();
        terminal.enterPrivateMode();
    }

    @Override
    public void draw(int x, int y, Status status) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss:SSS");
    }
}
