package com.yegor;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import javax.swing.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        Terminal terminal = new DefaultTerminalFactory().createTerminal();
        terminal.clearScreen();
        terminal.setCursorPosition(TerminalPosition.TOP_LEFT_CORNER);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss:SSS");
        terminal.setCursorVisible(false);
        terminal.enterPrivateMode();
        while(true) {
            KeyStroke keyStroke = terminal.pollInput();
            if(keyStroke != null && (keyStroke.getKeyType() == KeyType.Escape) || (keyStroke.getKeyType() == KeyType.EOF)) break;

            terminal.setCursorPosition(TerminalPosition.TOP_LEFT_CORNER);

            LocalDateTime time = LocalDateTime.now();
            print(terminal, time.format(dateTimeFormatter));

            Thread.sleep(1);
        }
        terminal.enterPrivateMode();
        terminal.close();
    }

    public static void print(Terminal terminal, String s) throws IOException {
        for(int i=0; i < s.length(); i++) {
            terminal.putCharacter(s.charAt(i));
        }
        terminal.flush();
    }
}
