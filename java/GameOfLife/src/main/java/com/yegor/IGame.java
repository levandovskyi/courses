package com.yegor;

public interface IGame {
    void init();

    void move();

    void draw(IDraw context);
}
