package com.yegor;

import com.Status;

public interface IDraw {
    void draw(int x, int y, Status status);
}
