package com;

import com.yegor.IDraw;
import com.yegor.IGame;

public class Game implements IGame {

    @Override
    public void init() {

    }

    @Override
    public void move() {

    }

    @Override
    public void draw(IDraw context) {
        context.draw(0, 0, Status.ALIVE);
    }

}
