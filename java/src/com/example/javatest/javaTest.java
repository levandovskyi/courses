package com.example.javatest;
import java.util.Scanner;

public class javaTest {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Create a triangle. Enter 3 sides values: ");
//        int a = scan.nextInt();
////        int b = scan.nextInt();
////        int c = scan.nextInt();
        int a = 10;
        int b = 10;
        int c = 10;
        String type;

        if (a + b > c && a + c > b && c + b > a) {
            System.out.println("This triangle exist");
            System.out.println("Side a: " + a + " cm");
            System.out.println("Side b: " + b + " cm");
            System.out.println("Side c: " + c + " cm");
            int sp = (a + b + c) / 2;
            double res = sp * (sp - a) * (sp - b) * (sp - c);
            double area = Math.sqrt(res);
            System.out.println("Area is " + area + " sqcm");
            if(a == b && a == c && c == b) {
                type = "Equal sides and angles";
                System.out.println(type);
            } else if (Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2) || Math.pow(c, 2) + Math.pow(b, 2) == Math.pow(a, 2) ||
                    Math.pow(a, 2) + Math.pow(c, 2) == Math.pow(b, 2)) {
                type = "Right triangle";
                System.out.println(type);
            } else {
                type = "All sides are different";
                System.out.println(type);
            }
        } else {
            System.out.println("This triangle does not exist");
        }

        int sum = 0;
        for(int i=1; i <= 100; i++) {
            sum += i;
        }

        System.out.println("1 + 2 ... + 100 = " + sum);
    }


}
