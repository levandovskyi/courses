import java.util.ArrayList;

public class Ship {
    private String name;
    private int lives;
    private boolean isAlive;
    private ArrayList<Location> length;

    public Ship(int lives, Location[][] board) {
        this.lives = lives;
        this.isAlive = true;
        length = new ArrayList<>(lives);
    }


    public int getLives() {
        return lives;
    }

    public int getLength() {
        return length.size();
    }

    public int getShipCoord(int a) {
        length.get(a).setStatus(4);
        return length.get(a).getStatus();
    }

    public void setLives(Location[][] board, int x, int y) {
        this.length.add(board[x][y]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public boolean isAlive() {
        return isAlive;
    }

    public void hit(boolean wasHit) {
        if(wasHit)
            this.lives--;
    }

    public void dead() {
        if(lives == 0) {
            this.isAlive = false;
        }
    }
}
