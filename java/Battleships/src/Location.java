public class Location {
    private boolean hasShip;
    private boolean wasHit;
    private boolean miss;
    private int status;
    private Location[][] location;
    private String name;

    public Location() {
        this.status = -1;
    }

    public boolean check(int x, int y) {
        if(location[x][y].hasShip)
            return true;
        return false;
    }

    public void setHasShip(boolean hasShip) {
        this.hasShip = hasShip;
    }

    public boolean isHasShip() {
        return hasShip;
    }

    public int getStatus() {
        return status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLocation(int x, int y) {
       return "null";
    }

    public boolean isWasHit() {
        return wasHit;
    }

    public void setWasHit(boolean wasHit) {
        this.wasHit = wasHit;
    }

    public boolean isMiss() {
        return miss;
    }

    public void setMiss(boolean miss) {
        this.miss = miss;
    }

    public Location[][] getLocation() {
        return location;
    }

    public void setLocation(Location[][] location) {
        this.location = location;
    }
}
