import com.sun.xml.internal.bind.v2.TODO;

public class Board {
    private Location[][] board;

    public Board() {
        this.board = new Location[10][10];
        for(int i=0; i < board.length; i++) {
            for(int j=0; j < board[i].length; j++) {

                this.board[i][j] = new Location();
            }
        }
    }

    public void display() {
        System.out.println("  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |");
        System.out.println("--+---+---+---+---+---+---+---+---+---+---|");
        StringBuilder sb = new StringBuilder();
        for(int i=0; i < board.length; i++) {
            sb.delete(0, sb.length());
            sb.append(i + " |");
            for(int j=0; j < board[i].length; j++) {
                if(this.board[i][j].isWasHit()) {
                    sb.append("\033[31;1m X\033[0m |");
                } else if (this.board[i][j].isHasShip()){
                    sb.append(" O |");
                } else if (this.board[i][j].isMiss()){
                    sb.append(" * |");
                } else {
                    sb.append(" \033[34m~ \033[0m|");
                }
        }
            System.out.println(sb.toString());
            System.out.println("--+---+---+---+---+---+---+---+---+---+---|");
        }
    }

    public void addShipVertical(int x, int y, int size) {
            Ship s = new Ship(size, board);
            for(int i=0; i < size; i++) {
                board[i + y][x].setHasShip(true);
                setName(i + y, x, size);
                s.setLives(board, y, i + x);
        }
    }

    public void addShipHorizontal(int x, int y, int size) {
        Ship s = new Ship(size, board);
        for(int i=0; i < size; i++) {
            board[y][i + x].setHasShip(true);
            setName(y,i + x, size);
            s.setLives(board, y, i + x);
        }
    }

    public void setName(int x, int y, int size) {
        switch (size){
            case 1:
                board[x][y].setName("destroyer");
                break;
            case 2:
                board[x][y].setName("submarine");
                break;
            default:
                board[x][y].setName("null");
        }
    }

    public String returnName(int x, int y) {
        return board[y][x].getName();
    }

    public void attack(int x, int y) {
        if(this.board[x][y].isHasShip()) {
            this.board[x][y].setWasHit(true);
        } else {
            this.board[x][y].setMiss(true);
        }
    }

    //TODO: random ship placement


}
