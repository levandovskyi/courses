public class Tournament {
    String name = "\nNext round";

    public void tournament(Match[] arr) {
        if (arr == null) return;
        if (arr.length == 1) {
            System.out.println("\nFinals");
            System.out.println(arr[0].getFirst() + " VS " + arr[0].getSecond());
            return;
        }

        int brackets = arr.length / 2;
        int id = arr[arr.length - 1].getId() + 1;
        Match[] round = new Match[brackets];

        for (int i = 0; i < brackets; i++) {
            round[i] = new Match(id++, arr[i * 2].getStringId(), arr[i * 2 + 1].getStringId());
        }

        if (round.length == 2) {
            name = "\nSemifinals";
        } else if (round.length == 4) {
            name = "\nQuarterfinals";
        } else if (round.length == 8) {
            name = "\nEighth-finals";
        }

        if (round.length > 1) {
            System.out.println(name);
            for (Match match : round) {
                System.out.println("[" + match.getStringId() + "] " + match.getFirst() + " VS " + match.getSecond());
            }
        }
        tournament(round);
    }


    public Match[] create(int people) {
        if (people <= 0 || people < 4) {
            System.out.println("Invalid amount of competitors");
            return null;
        }
        System.out.println("There are " + people + " competitors");
        Match[] firstRound = new Match[people / 2];
        for (int i = 0; i < firstRound.length; i++) {
            firstRound[i] = new Match(i, String.valueOf(i * 2 + 1), String.valueOf(i * 2 + 2));
        }

        for (Match match : firstRound) {
            System.out.println("[" + match.getStringId() + "] " + match.getFirst() + " VS " + match.getSecond());
        }

        return firstRound;
    }
}
