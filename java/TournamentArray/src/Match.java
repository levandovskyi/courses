public class Match {
    private int id;
    private String first;
    private String second;

    public Match(int id, String first, String second) {
        this.id = id;
        this.first = first;
        this.second = second;
    }

    public String getStringId() {
        return "ID" + id;
    }

    public int getId() {
        return id;
    }

    public String getFirst() {
        return first;
    }


    public String getSecond() {
        return second;
    }

}
