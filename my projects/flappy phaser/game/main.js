let config = {
    type: Phaser.AUTO,
    width: 288,
    height: 512,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 300},
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

let game = new Phaser.Game(config);
let ground;
let bird;
let cursors;
let bg;
let pipes;

function preload() {
    this.load.image('bg', '../img/bg.png');
    this.load.image('fg', '../img/fg.png');
    this.load.spritesheet('bird', '../img/spritesheet/spritesheet.png', {frameWidth: 44, frameHeight: 30});
    this.load.image('pipeUp', '../img/pipeNorth.png');
    this.load.image('pipeDown', '../img/pipeSouth.png');
    cursors = this.input.keyboard.createCursorKeys();

}

function create() {

    this.add.image(0, 0, 'bg').setOrigin(0, 0);
    ground = this.physics.add.staticGroup();
    pipes = this.physics.add.staticGroup();
    pipes.create(150, 0, 'pipeUp');
    ground.create(150, 480, 'fg');
    bird = this.physics.add.sprite(200, 200, 'bird');
    bird.setBounceY(0.5);
    bird.setBounceX(0.5);
    bird.setMass(20);


    bird.body.setGravityY(300);
    bird.setCollideWorldBounds(true);
    this.physics.add.collider(bird, ground);
    this.physics.add.collider(bird, pipes);

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('bird', {start: 0, end: 2}),
        frameRate: 5,
        repeat: -1
    });

    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('bird', {start: 3, end: 5}),
        frameRate: 5,
        repeat: -1
    });
}

function update() {
    if (cursors.left.isDown) {
        bird.setVelocityX(-160);
        bird.anims.play('left', true);
    } else if (cursors.right.isDown) {
        bird.setVelocityX(160);
        bird.anims.play('right', true);
    } else if (cursors.up.isDown) {
        bird.setVelocityY(-160);
    } else {
        bird.setVelocityX(0);
    }
}