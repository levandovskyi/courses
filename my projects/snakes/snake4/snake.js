class Snake {
  constructor(scale) {
    this.scale = scale;
    this.direction = null;
    this.body = [{ row: 1, col: 1 }];
    this.score = 0;
  }

  draw() {
    for (let i = 0; i < this.body.length; i++) {
      ctx.fillStyle = 'black';
      ctx.fillRect(
        this.body[i].row * this.scale,
        this.body[i].col * this.scale,
        this.scale,
        this.scale
      );
      ctx.strokeStyle = 'white';
      ctx.strokeRect(
        this.body[i].row * this.scale,
        this.body[i].col * this.scale,
        this.scale,
        this.scale
      );
    }
  }

  getDir() {
    if (this.direction == 'UP') {
      return { row: -1, col: 0 };
    } else if (this.direction == 'DOWN') {
      return { row: 1, col: 0 };
    } else if (this.direction == 'LEFT') {
      return { row: 0, col: -1 };
    } else if (this.direction == 'RIGHT') {
      return { row: 0, col: 1 };
    }
    return false;
  }

  moveSnake() {
    if (!this.getDir()) return;

    this.body.unshift({
      col: this.body[0].col,
      row: this.body[0].row
    });

    this.body[0].col += this.getDir().row;
    this.body[0].row += this.getDir().col;

    this.body.pop();
  }

  ateFood(foodX, foodY) {
    if (this.body[0].row === foodX && this.body[0].col === foodY) {
      return true;
    }
  }

  newPart() {
    this.body.unshift({
      col: this.body[0].col,
      row: this.body[0].row
    });
    this.score++;
  }

  collision(board) {
    if (this.body[0].col >= board.cols) {
      return true;
    } else if (this.body[0].col < 0) {
      return true;
    } else if (this.body[0].row >= board.rows) {
      return true;
    } else if (this.body[0].row < 0) {
      return true;
    }
  }

  ateSelf() {
    let newHead = {
      col: this.body[0].col,
      row: this.body[0].row
    };
    for (let i = 2; i < this.body.length; i++) {
      if (
        newHead.col === this.body[i].col &&
        newHead.row === this.body[i].row
      ) {
        return true;
      }
    }
  }

}
