/**
 * @type {HTMLCanvasElement}
 */
let cvs = document.getElementById('canvas');
let ctx = cvs.getContext('2d');
let menu = document.getElementById('menu');
let start = document.getElementById('start');
let restart = document.getElementById('restart');
let score = document.getElementById('score');
let time = null;
let player = document.getElementById('name');
let addScore = document.getElementById('addScore');

// score.innerHTML = snake.score;

cvs.style.margin = '0';

cvs.width = '400';
cvs.height = '400';

//arrows
let up = 38;
let down = 40;
let left = 37;
let right = 39;

//time variables
// let previousFrameTime;
// let deltaTime;

let fps = 10;

//game scale
let scale = 20;

//create empty board
let board = new Board(20, 20, scale);
board.createBoard();

//create snake
let snake = new Snake(1 * scale, 1 * scale, scale);

//create food
let food = new Food(1 * scale, 1 * scale, scale);

//update elements function
function update() {
  ctx.clearRect(0, 0, cvs.width + 20, cvs.height + 20);


  // board.drawBoard();

  snake.draw();
  food.drawFood();

  snake.moveSnake(food.foodX, food.foodY);
  if (snake.ateFood(food.foodX, food.foodY)) snake.newPart();
  score.innerHTML = `Score: ${snake.score}`;
  food.newFood(snake.ateFood(food.foodX, food.foodY));
  food.foodOnSnake(snake.body);

  if (snake.collision(board) || snake.ateSelf()) {
    clearInterval(time);
    time = null;
    restart.style.visibility = 'visible';
  }

}


addScore.addEventListener('click', () => {
  highscores.push({
    name: player.value, 
    score: snake.score
  });
  saveLocal();
  drawToLdb();
})

function startGame() {
  if (!time) {
    time = setInterval(update, 200);
  }
}

restart.addEventListener('click', () => {
  window.location.reload();
});

let key = null;
//controls
document.addEventListener('keydown', e => {
  let keyCode = e.keyCode;

  if (keyCode == up && snake.direction != 'DOWN') {
    snake.direction = 'UP';
  } else if (keyCode == down && snake.direction != 'UP') {
    snake.direction = 'DOWN';
  } else if (keyCode == left && snake.direction != 'RIGHT') {
    snake.direction = 'LEFT';
  } else if (keyCode == right && snake.direction != 'LEFT') {
    snake.direction = 'RIGHT';
  }
});


startGame();
