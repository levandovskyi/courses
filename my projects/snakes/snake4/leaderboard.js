let leaderboard = document.getElementById('leaderboard');

let highscores = [{ name: 'Dev', score: (1 / 0).toString() }];

function saveLocal() {
  localStorage.setItem('highscores', JSON.stringify(highscores));
}

function readLocal() {
  let value = localStorage.getItem('highscores');
  if (value === null) {
    saveLocal();
  } else {
    highscores = JSON.parse(value);
  }
}

// let getHighscores = JSON.parse(localStorage.getItem('highscores'));

function drawToLdb() {
  leaderboard.innerHTML = '';
  for (let i = 0; i < highscores.length; i++) {
    leaderboard.innerHTML += `
    <table>
      <tr>
        <td>${highscores[i].name}</td>
        <td>${highscores[i].score}</td>
      </tr>
  </table>`;
  }
}

readLocal();

drawToLdb();
