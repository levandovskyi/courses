let cvs = document.createElement('canvas');
let ctx = cvs.getContext('2d');
let body = document.getElementById('body');
body.append(cvs);

//score
let score = 0;

//arrows
let up = 38;
let down = 40;
let left = 37;
let right = 39;

//frames per second of the game
let fps = 10;

//reset body style
body.style.margin = '0';

//grid scale
let scale = 20;

let board = new Board(30, 30, 20);

board.createBoard();

//canvas element details
cvs.width = 600;
cvs.height = 600;
cvs.style.backgroundColor = '#ffccff';

//snake
let snake = new Snake(1, 1,[], 'LEFT');





//food box details
let col = Math.floor(cvs.width / scale);
let row = Math.floor(cvs.height / scale);



//random food position
let foodX = Math.floor(Math.random() * row);
let foodY = Math.floor(Math.random() * col);

//random food on a canvas
function drawFood() {
  ctx.beginPath();
  ctx.fillStyle = 'yellow';
  ctx.fillRect(foodX * scale, foodY * scale, scale, scale);
  ctx.stroke();
};

//switch key
let key = 'empty';

//game function
function game() {
  setTimeout(function() {
    requestAnimationFrame(game);
    ctx.clearRect(0, 0, cvs.width, cvs.height);

    drawFood();
    snake.draw(scale);
    snake.tail[0] = {
      row: snake.row,
      col: snake.col - 1
    };
    ctx.fillRect(snake.tail[0].col * scale, snake.tail[0].row * scale, scale, scale)

    //conditions for animation control out of the loop.
    switch (key) {
      case 'RIGHT':
        snake.col++;
        break;
      case 'LEFT':
        snake.col--;
        break;
      case 'UP':
        snake.row--;
        break;
      case 'DOWN':
        snake.row++;
        break;
      default:
        break;
    }

    
    //collision with walls detection
    if (snake.row > col -1) {
      snake.row = 0;
    } else if (snake.row < 0) {
      snake.row = col - 1;
    } else if (snake.col < 0) {
      snake.col = row - 1;
    } else if (snake.col > row - 1) {
      snake.col = 0;
    }
    // console.table(board.grid);

    if (snake.foodLoc(foodY, foodX)) {
      foodX = Math.floor(Math.random() * col);
      foodY = Math.floor(Math.random() * row);
      score++;

      console.clear();
      console.log(score);
      console.log(snake.tail);
    };

  }, 1000 / fps);
}

// controls
document.addEventListener('keydown', e => {
  if (e.keyCode === up) {
    key = 'UP';
    snake.direction = 'UP';
    board.grid[snake.row][snake.col] = snake.direction;
  } else if (e.keyCode === down) {
    key = 'DOWN';
    snake.direction = 'DOWN';
    board.grid[snake.row][snake.col] = 'DOWN';
  } else if (e.keyCode === left) {
    key = 'LEFT';
    snake.direction = 'LEFT';
    board.grid[snake.row][snake.col] = 'LEFT';
  } else if (e.keyCode === right) {
    key = 'RIGHT';
    snake.direction = 'RIGHT';
    board.grid[snake.row][snake.col] = 'RIGHT';
  }

  console.log(key);
});



//game start
game();