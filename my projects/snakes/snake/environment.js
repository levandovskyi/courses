class Board {
  constructor(cols, rows, scale) {
    this.cols = cols;
    this.rows = rows;
    this.scale = scale;
    this.grid = [];
  }

  createBoard() {
    for (let row = 0; row < this.rows; row++) {
      this.grid[row] = [];
      for (let col = 0; col < this.cols; col++) {
        this.grid[row][col] = null;
      }
    }
  }
}
