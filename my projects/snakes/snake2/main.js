let cvs = document.createElement('canvas');
let ctx = cvs.getContext('2d');
let body = document.getElementById('body');
body.append(cvs);


body.style.margin = '0';

cvs.width = '600';
cvs.height = '600';

//arrows
let up = 38;
let down = 40;
let left = 37;
let right = 39;

//game scale
let scale = 20;

//create game objects
let board = new Board(30, 30, scale);
let snake = new Snake(1 * scale, 1 * scale, scale, scale);

let fps = 10;

board.createBoard();

 //key switch
let key = '';


function moving() {
  for(let i=0; i < snake.tail.length; i++) {
    let tailPartPos = board.grid[snake.tail[i].row][snake.tail[i].col];
    if(tailPartPos != null){
      snake.tail[i].row--;
      snake.tail[i].d = tailPartPos;
    } else {
      if(snake.tail[i].d == 'UP') {
        snake.tail[i].row--;
      }
    }
  }
}






function game() {
  setTimeout(() => {
    ctx.clearRect(0, 0, cvs.width, cvs.height);


    //object functions: 1) create board and draw it 2) draws snake on a board
    board.drawBoard();
    snake.drawSnake();
    snake.drawPart();

    switch (key) {
      case 'RIGHT':
        snake.col++;
        snake.isMoving = true;

        break;
      case 'LEFT':
        snake.col--;
        snake.isMoving = true;
        moving();
        break;
      case 'UP':
        snake.row--;
        snake.isMoving = true;
        moving();
        break;
      case 'DOWN':
        snake.row++;
        snake.isMoving = true;
        moving();
        break;
      default:
      snake.isMoving = false;
        break;
    }

    if (snake.col >= board.cols) {
      snake.col -= 1;
    } else if (snake.col < 0) {
      snake.col += 1;
    } else if (snake.row >= board.rows) {
      snake.row -= 1;
    } else if (snake.row < 0) {
      snake.row += 1;
    }

    document.addEventListener('keydown', e => {
      if (e.keyCode === up) {
        key = 'UP';
        snake.direction = key;
        board.grid[snake.row][snake.col] = key;
      } else if (e.keyCode === down) {
        key = 'DOWN';
        snake.direction = key;
        board.grid[snake.row][snake.col] = key;
      } else if (e.keyCode === left) {
        key = 'LEFT';
        snake.direction = key;
        board.grid[snake.row][snake.col] = key;
      } else if (e.keyCode === right) {
        key = 'RIGHT';
        snake.direction = key;
        board.grid[snake.row][snake.col] = key;
      } 
    
     
    });
  
    requestAnimationFrame(game);
  }, 1000/fps);


}



game();
