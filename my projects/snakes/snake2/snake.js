class Snake {
  constructor(col, row, scale) {
    this.col = col;
    this.row = row;
    this.scale = scale;
    this.direction = '';
    this.total = 2;
    this.tail = [{col: this.col + 1, row: this.row, d: 'LEFT'}, {col: this.col + 2, row: this.row, d: 'LEFT'} ];
    this.isMoving = false;
  }

  drawSnake() {
    ctx.fillStyle = '#000';
    ctx.fillRect(this.col * this.scale, this.row * this.scale ,this.scale, this.scale);
  }

  drawPart() {
    for(let i=0; i < this.tail.length; i++) {
      ctx.fillRect(this.tail[i].col * scale, this.tail[i].row * scale, this.scale, this.scale);
    }
    
  }

}