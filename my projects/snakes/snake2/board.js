class Board {
  constructor(cols, rows, scale) {
    this.cols = cols;
    this.rows = rows;
    this.scale = scale;
    this.grid = [];
  }

  createBoard() {
    for (let row = 0; row < this.rows; row++) {
      this.grid[row] = [];
      for (let col = 0; col < this.cols; col++) {
        this.grid[row][col] = null;
      }
    }
  }

  drawBoard() {
    ctx.strokeStyle = '#808080';
    ctx.beginPath();
    for(let row = 0; row <= this.rows; row++) {
      ctx.moveTo(0, row * this.scale);
      ctx.lineTo(this.cols  * this.scale, row * this.scale);
      for (let col = 0; col <= this.cols; col++) {
        ctx.moveTo(col * this.scale, 0);
        ctx.lineTo(col * this.scale, this.rows * this.scale);
      }
    }
    ctx.stroke();
  }
}
