class Food {
  constructor(col, row, scale) {
    this.col = col;
    this.row = row;
    this.scale = scale;
    this.foodX = Math.floor(Math.random() * this.row);
    this.foodY = Math.floor(Math.random() * this.col);
  }


  drawFood() {
    ctx.beginPath();
    ctx.fillStyle = 'yellow';
    ctx.fillRect(this.foodX * this.scale, this.foodY * this.scale, this.scale, this.scale);
    ctx.closePath();
  }

  newFood(wasEaten) {
    if (wasEaten === true) {
      this.foodX = Math.floor(Math.random() * this.row);
      this.foodY = Math.floor(Math.random() * this.col);
    }
  }
}