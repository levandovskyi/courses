class Snake {
  constructor(col, row, scale) {
    this.col = col;
    this.row = row;
    this.scale = scale;
    this.direction = null;
    this.body = [
      { row: this.row + 1, col: this.col, direction: null },
      { row: this.row + 2, col: this.col, direction: null },
      { row: this.row + 3, col: this.col, direction: null }
    ];
    this.total = 2;
  }

  drawHead() {
    ctx.fillStyle = 'black';
    ctx.fillRect(
      this.row * this.scale,
      this.col * this.scale,
      this.scale,
      this.scale
    );
  }

  drawBody() {
    for (let i = 0; i < this.body.length; i++) {
      ctx.fillStyle = 'black';
      ctx.fillRect(
        this.body[i].row * scale,
        this.body[i].col * scale,
        this.scale,
        this.scale
      );
    }
  }

  getDir() {
    if (this.direction == 'UP') {
      return { row: -1, col: 0 };
    } else if (this.direction == 'DOWN') {
      return { row: 1, col: 0 };
    } else if (this.direction == 'LEFT') {
      return { row: 0, col: -1 };
    } else if (this.direction == 'RIGHT') {
      return { row: 0, col: 1 };
    }
    return false;
  }

  collision(board) {
    if (this.col >= board.cols) {
      this.col -= 1;
    } else if (this.col < 0) {
      this.col += 1;
    } else if (this.row >= board.rows) {
      this.row -= 1;
    } else if (this.row < 0) {
      this.row += 1;
    }
  }

  moveHead() {
    if (!this.getDir()) return;
    this.col += this.getDir().row;
    this.row += this.getDir().col;
  }

  moveSnakeBody(board) {
    for (let i = 0; i < this.body.length; i++) {
      this.body[i].direction = board.grid[this.body[i].col][this.body[i].row];

      if (board.grid[this.body[i].col][this.body[i].row] == null) {
        this.body[i].direction = this.direction;
      }

      if(i === this.body.length - 1) {
        let last = this.body[this.body.length - 1];
        let prevDir = last.direction;
        board.grid[last.col][last.row] = null;
        last.direction == prevDir;
      }

      if (this.body[i].direction == 'UP') {
        this.body[i].col--;
      } else if (this.body[i].direction == 'DOWN') {
        this.body[i].col++;
      } else if (this.body[i].direction == 'LEFT') {
        this.body[i].row--;
      } else if (this.body[i].direction == 'RIGHT') {
        this.body[i].row++;
      }
    }

  }

  writeDir(board) {
    if (board.grid[this.col][this.row] == null) {
      board.grid[this.col][this.row] = this.direction;
    }
  }

  ateFood(foodY, foodX) {
    if(this.col === foodY && this.row === foodX) {
      this.total++;
      for(let i=0; i < this.total; i++){

    } 



      return true;
    }
  }
}
