let cvs = document.createElement('canvas');
let ctx = cvs.getContext('2d');
let body = document.getElementById('body');
body.append(cvs);


body.style.margin = '0';

cvs.width = '600';
cvs.height = '600';

//arrows
let up = 38;
let down = 40;
let left = 37;
let right = 39;

//time variables
// let previousFrameTime;
// let deltaTime;

let fps = 10;

//game scale
let scale = 20;

//create empty board
let board = new Board(30, 30, scale);
board.createBoard();

//create snake
let snake = new Snake(1 * scale, 1 * scale, scale);

//create food
let food = new Food(1 * scale, 1 * scale, scale);


//update elements function
function update() {
  ctx.clearRect(0, 0, cvs.width, cvs.height);
  board.drawBoard();
  snake.drawHead();
  snake.drawBody();
  food.drawFood();
  snake.moveHead();
  snake.moveSnakeBody(board);
  food.newFood(snake.ateFood(food.foodY, food.foodX));
  snake.addPart();
  snake.collision(board);
  snake.writeDir(board);
}



//draw on canvas
function draw() {
  setTimeout(() => {
    requestAnimationFrame(draw);
    update();
  }, 1000/fps)
}


stop();
let key = null;
//controls
document.addEventListener('keydown', (e) => {
  let keyCode = e.keyCode;
  
  if(keyCode == up) {
    snake.direction = 'UP';
  } else if (keyCode == down) {
    snake.direction = 'DOWN'; 
  } else if (keyCode == left) {
    snake.direction = 'LEFT';
  } else if (keyCode == right) {
    snake.direction = 'RIGHT';
  }

  if(keyCode) {
    board.grid[snake.col][snake.row] = snake.direction;
  }
});



draw();

// function frame() {
//   let snakeFrameTime = Date.now();
//   deltaTime = snakeFrameTime - previousFrameTime;

//   update();

//   previousFrameTime = snakeFrameTime;
//   window.requestAnimationFrame(frame);
// }


// previousFrameTime = Date.now();
// window.requestAnimationFrame(frame);
